port: {PORT}

omero:
    host: "{OMERO_HOST}"
    port: {OMERO_PORT}

omero.server:
    omero.data.dir: "{OMERO_DATA_DIR}"
    omero.db.host: "{DB_HOST}"
    omero.db.name: "{DB_NAME}"
    omero.db.port: "{DB_PORT}"
    omero.db.user: "{DB_USER}"
    omero.db.pass: "{DB_PASS}"
    omero.script_repo_root: "{OMERO_SCRIPT_ROOT}"
    omero.pixeldata.max_tile_length: "2048"

omero.web:
    session_cookie_name: "{OMERO_COOKIE_NAME}"

session-store:
    type: "{SESSION_STORE_TYPE}"
    synchronicity: "async"
    uri: "{SESSION_STORE_URI}"

redis-cache:
    uri: "{CACHE_URI}"

# Image region cache configuration.  Uses the "redis-cache" configured above
# to avoid round trips to the server when the same region has already been
# rendered.
image-region-cache:
    enabled: {CACHE_ENABLED}

# Pixels metadata cache configuration.  Uses the "redis-cache" configured above
# to avoid round trips to the server to retrieve the same set of Pixels
# metadata.
pixels-metadata-cache:
    enabled: {CACHE_ENABLED}

cache-control-header: "private, max-age=3600"
