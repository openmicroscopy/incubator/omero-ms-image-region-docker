FROM gradle:5.4.1-jdk11 AS builder

WORKDIR /home/gradle/omero-ms-image-region
RUN curl -sSfL https://github.com/glencoesoftware/omero-ms-image-region/archive/afec67676104c915a842dda905fb3496fa191137.tar.gz | \
    tar -zx --strip=1
RUN gradle --no-daemon build && \
    cd build/distributions && \
    tar xf omero-ms-image-region-0.5.0-SNAPSHOT.tar


################################################################################
FROM openjdk:11.0.3-jre-stretch

LABEL maintainer="ome-devel@lists.openmicroscopy.org.uk"
LABEL org.opencontainers.image.created="unknown"
LABEL org.opencontainers.image.revision="unknown"
LABEL org.opencontainers.image.source="https://github.com/ome/omero-ms-image-region-docker"

RUN apt-get update -q && \
    apt-get install -q -y python3-minimal && \
    apt-get clean
RUN useradd -m -d /opt/omero-ms omero-ms
WORKDIR /opt/omero-ms
USER omero-ms
COPY --from=builder --chown=omero-ms /home/gradle/omero-ms-image-region/build/distributions/omero-ms-image-region-0.5.0-SNAPSHOT omero-ms-image-region
RUN rm /opt/omero-ms/omero-ms-image-region/conf/config.yaml

ADD config.yaml.template /opt/omero-ms/omero-ms-image-region/conf/
ADD entrypoint.py /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/entrypoint.py"]
