#!/usr/bin/python3

import os
import sys

CONFIG = '/opt/omero-ms/omero-ms-image-region/conf/config.yaml'


def setdefault(env, var, default=None):
    if var not in env:
        if default is None:
            sys.stderr.write(
                'Required configuration property {var} missing\n'.format(
                    var=var))
            sys.exit(1)
        env[var] = default
    return env[var]


if os.path.exists(CONFIG):
    sys.stderr.write('Using existing configuration {}\n'.format(CONFIG))
    sys.exit(0)

env = dict(os.environ)
setdefault(env, 'PORT', '8080')

setdefault(env, 'OMERO_HOST', 'omeroserver')
setdefault(env, 'OMERO_PORT', '4064')

setdefault(env, 'OMERO_DATA_DIR', '/OMERO')
setdefault(env, 'DB_HOST', 'omero')
setdefault(env, 'DB_NAME', 'omero')
setdefault(env, 'DB_PORT', '5432')
setdefault(env, 'DB_USER', 'omero')
setdefault(env, 'DB_PASS', 'omero')
setdefault(env, 'OMERO_SCRIPT_ROOT', '')

setdefault(env, 'OMERO_COOKIE_NAME', 'sessionid')

s = setdefault(env, 'SESSION_STORE_URI', None)
if s.startswith('redis:'):
    setdefault(env, 'SESSION_STORE_TYPE', 'redis')
else:
    setdefault(env, 'SESSION_STORE_TYPE', 'postgres')

r = setdefault(env, 'CACHE_URI', '')
setdefault(env, 'CACHE_ENABLED', str(bool(r)).lower())

setdefault(env, 'REDIS_CACHE_URI', 'omero')

with open(CONFIG + '.template') as f:
    cfg = f.read()
with open(CONFIG, 'w') as f:
    f.write(cfg.format(**env))

# https://docs.python.org/3/library/os.html#os.execv
args = ['omero-ms-image-region'] + sys.argv[1:]
sys.stdout.flush()
sys.stderr.flush()
os.execv('/opt/omero-ms/omero-ms-image-region/bin/omero-ms-image-region', args)
